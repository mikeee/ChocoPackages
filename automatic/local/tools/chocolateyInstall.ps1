﻿$ErrorActionPreference = 'Stop'

$packageArgs = @{
    packageName    = $env:ChocolateyPackageName
    url            = 'https://cdn.localwp.com/releases-stable/6.7.0+6347/local-6.7.0-windows.exe'
    checksum       = '99eba379e33bcd95b2484915915605e3515ae0798441f789610af4486568778a'
    checksumType   = 'SHA256'
    fileType       = 'EXE'
    silentArgs     = '/S'
    validExitCodes = @(0)
}

Install-ChocolateyPackage @packageArgs
